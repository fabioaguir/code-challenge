import { Injectable } from "@angular/core";
import { Parede } from "./components/parede/parede.model";
import { GrupoParede } from "./grupo-parede";
import { MedidasPadrao } from "./resource/enuns";

export interface LatasTinta {
  lata18L: number
  lata3L6M: number
  lata2L5M: number
  lata05M: number
}

@Injectable()
export class AppService {
  latasTinha = {} as LatasTinta

  quantidadeLatasDeTinta(paredes: GrupoParede): LatasTinta {
    const portas = Number(paredes.paredeUm.portas) + Number(paredes.paredeDois.portas)
    + Number(paredes.paredeTres.portas) + Number(paredes.paredeQuatro.portas)
    const areaPortas = portas * (MedidasPadrao.LarguraPorta * MedidasPadrao.AlturaPorta)

    const janelas = Number(paredes.paredeUm.janelas) + Number(paredes.paredeDois.janelas)
    + Number(paredes.paredeTres.janelas) + Number(paredes.paredeQuatro.janelas)
    const areaJanelas = janelas * (MedidasPadrao.LarguraJanela * MedidasPadrao.AlturaJanela)

    const areaParedes = this.calcularAreaParede(paredes.paredeUm) +
     this.calcularAreaParede(paredes.paredeDois)
     + this.calcularAreaParede(paredes.paredeTres)
     + this.calcularAreaParede(paredes.paredeQuatro)

    const areaRealParedes = areaParedes - (areaPortas + areaJanelas)

    let litrosDeTinta = areaRealParedes / MedidasPadrao.LitroPorMetro
    this.latasTinha.lata18L = Math.floor(litrosDeTinta / MedidasPadrao.LitroLata18)

    litrosDeTinta = litrosDeTinta - (MedidasPadrao.LitroLata18 * this.latasTinha.lata18L)
    this.latasTinha.lata3L6M = Math.floor(litrosDeTinta / MedidasPadrao.LitroLata3L6M)

    litrosDeTinta = litrosDeTinta - (MedidasPadrao.LitroLata3L6M * this.latasTinha.lata3L6M)
    this.latasTinha.lata2L5M = Math.floor(litrosDeTinta / MedidasPadrao.LitroLata2L5M)

    litrosDeTinta = litrosDeTinta - (MedidasPadrao.LitroLata2L5M * this.latasTinha.lata2L5M)
    this.latasTinha.lata05M = Math.floor(litrosDeTinta / MedidasPadrao.LitroLata05M)

    return this.latasTinha
  }

  calcularAreaParede(parede: Parede) {
    return Number(parede.largura) * Number(parede.altura)
  }
}
