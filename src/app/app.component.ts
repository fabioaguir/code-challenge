import { Component } from '@angular/core';
import { GrupoParede } from './grupo-parede';
import { Parede } from './components/parede/parede.model';
import { MedidasPadrao } from './resource/enuns';
import { AppService, LatasTinta } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  paredes = {} as GrupoParede
  latasTinta: LatasTinta

  constructor(private appService: AppService) {}

  get paredesInformada() {
    return this.paredes.paredeUm && this.paredes.paredeDois && this.paredes.paredeTres && this.paredes.paredeQuatro
  }

  exibirLatasDeTinta() {
    this.latasTinta = this.appService.quantidadeLatasDeTinta(this.paredes)
  }
}
