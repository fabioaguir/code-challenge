import { Component, Input } from '@angular/core';

import { LatasTinta } from 'src/app/app.service';

@Component({
  selector: 'app-painel-tintas',
  templateUrl: './painel-tintas.component.html',
  styleUrls: ['./painel-tintas.component.scss']
})
export class PainelTintasComponent {
  @Input() latasTinta: LatasTinta
}
