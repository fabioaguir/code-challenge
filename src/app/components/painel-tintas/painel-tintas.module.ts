import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PainelTintasComponent } from './painel-tintas.component';



@NgModule({
  declarations: [PainelTintasComponent],
  imports: [
    CommonModule
  ],
  exports: [PainelTintasComponent]
})
export class PainelTintasModule { }
