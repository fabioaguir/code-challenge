import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PainelTintasComponent } from './painel-tintas.component';

describe('PainelTintasComponent', () => {
  let component: PainelTintasComponent;
  let fixture: ComponentFixture<PainelTintasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PainelTintasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PainelTintasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
