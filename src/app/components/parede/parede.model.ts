export interface Parede {
  largura: number,
  altura: number,
  portas: number,
  janelas: number,
}
