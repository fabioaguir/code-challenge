import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from '../input-text/input-text.module';
import { ParedeForm } from './parede-form';

import { ParedeComponent } from './parede.component';
import { ParedeService } from './parede.service';

describe('ParedeComponent', () => {
  let component: ParedeComponent;
  let fixture: ComponentFixture<ParedeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParedeComponent ],
      imports: [
        InputTextModule,
        FormsModule,
        ReactiveFormsModule],
        providers: [ParedeForm, ParedeService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
