import { Injectable } from "@angular/core";
import { MedidasPadrao } from "src/app/resource/enuns";
import { ParedeForm } from "./parede-form";
import { Parede } from "./parede.model";

@Injectable()
export class ParedeService {

  constructor(private paredeForm: ParedeForm) {}

  validarAreaParede(): boolean {
    const parede = this.paredeForm.parede as Parede
    const areaParede = parede.largura * parede.altura

    if (areaParede < 1 || areaParede > 50) {
      return false
    }

    return true
  }

  validarPorcentagemAreaPortaJanela() {
    const parede = this.paredeForm.parede as Parede
    const areaParede = parede.largura * parede.altura

    if (parede.portas || parede.janelas) {
      const areaPortas = parede.portas * (MedidasPadrao.LarguraPorta * MedidasPadrao.AlturaPorta)
      const areaJanelas = parede.janelas * (MedidasPadrao.LarguraJanela * MedidasPadrao.AlturaJanela)

      const areaPortaJanelas = areaPortas + areaJanelas

      if(areaPortaJanelas > (areaParede / 2)) {
        return false
      }
    }

    return true
  }

  validarAlturaParedeParaPorta() {
    const parede = this.paredeForm.parede as Parede

    if(parede.portas) {
      const diferencaAlturaParedePorta = parede.altura - MedidasPadrao.AlturaPorta
      if (diferencaAlturaParedePorta < 0.30) {
        return false
      }
    }

    return true
  }

}
