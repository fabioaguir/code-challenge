import { Component, Input, Optional, Self } from '@angular/core';
import { ControlValueAccessor,FormGroup, NgControl } from '@angular/forms';

import { ParedeForm } from './parede-form';
import { Parede } from './parede.model';
import { ParedeService } from './parede.service';

@Component({
  selector: 'app-parede',
  templateUrl: './parede.component.html',
  styleUrls: ['./parede.component.scss'],
  providers: [ParedeForm, ParedeService]
})
export class ParedeComponent implements ControlValueAccessor {

  @Input() titulo: string;

  form: FormGroup;
  mensagemAlerta: string
  paredeValida = false

  innerValue: Parede;

  onChange: any = () => {};
  onTouch: any = () => {};

  constructor(
    public paredeForm: ParedeForm,
    private paredeService: ParedeService,
    @Self() @Optional() public ngControl: NgControl) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this
    }
   }

   get value(): Parede {
    return this.innerValue
  }

  set value(value) {
    if (this.innerValue !== value) {
      this.innerValue = value;
      this.onChange(value);
      this.onTouch(value);
    }
  }

  salvar() {
    if (this.validacoes) {
      this.value = this.paredeForm.parede
      this.paredeValida = true
    } else {
      this.value = null
      this.paredeValida = false
    }
  }

  get validacoes() {

    if (!this.paredeService.validarAreaParede()) {
      this.mensagemAlerta = 'A parede não pode ter menos de 1 e mais do que 50 metros quadrados'
      return false
    } else {
      this.mensagemAlerta = null
    }

    if(!this.paredeService.validarPorcentagemAreaPortaJanela()) {
      this.mensagemAlerta = 'A área das portas e janelas deve ser até 50% da área da parede!'
      return false
    } else {
      this.mensagemAlerta = null
    }

    if (!this.paredeService.validarAlturaParedeParaPorta()) {
      this.mensagemAlerta = 'A altura da parede deve ser 30 centímetros maior que a altura da porta'
      return false
    } else {
      this.mensagemAlerta = null
    }

    return true
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
}
