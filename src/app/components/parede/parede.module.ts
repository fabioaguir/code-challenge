import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParedeComponent } from './parede.component';
import { InputTextModule } from '../input-text/input-text.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ParedeComponent],
  imports: [
    CommonModule,
    InputTextModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [ParedeComponent]
})
export class ParedeModule { }
