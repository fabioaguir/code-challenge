import { Injectable } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Parede } from "./parede.model";

@Injectable()
export class ParedeForm {
  formGroup: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.formGroup = formBuilder.group({
      largura: formBuilder.control(null, [Validators.required]),
      altura: formBuilder.control(null, [Validators.required]),
      portas: null,
      janelas: null,
    })
  }

  get parede() {
    return this.formGroup.value as Parede;
  }

  get largura(): AbstractControl {
    return this.formGroup.get('largura')
  }

  get altura(): AbstractControl {
    return this.formGroup.get('altura')
  }

  get portas(): AbstractControl {
    return this.formGroup.get('portas')
  }

  get janelas(): AbstractControl {
    return this.formGroup.get('janelas')
  }
}
