import { Component, EventEmitter, Input, OnInit, Optional, Output, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit, ControlValueAccessor {

  @Input() label: string
  @Input() id: string
  @Input() inputType = 'text'
  @Output() valueChange = new EventEmitter();

  innerValue = '';

  onChange: any = () => {};
  onTouch: any = () => {};

  constructor(@Self() @Optional() public ngControl: NgControl) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this
    }
   }

  ngOnInit(): void {
  }

  changed() {
    this.valueChange.emit()
  }

  get value(): string {
    return this.innerValue?.replace(',', '.')
  }

  set value(value) {
    if (this.innerValue !== value) {
      this.innerValue = value;
      this.onChange(value);
      this.onTouch(value);
    }
  }

  writeValue(obj: any): void {
    this.value = obj;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

}
