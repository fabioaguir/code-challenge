export enum MedidasPadrao {
  LarguraPorta = 0.8,
  AlturaPorta = 1.90,
  LarguraJanela = 2.0,
  AlturaJanela = 1.20,
  LitroPorMetro = 5,
  LitroLata18 = 18,
  LitroLata3L6M = 3.6,
  LitroLata2L5M = 2.5,
  LitroLata05M = 0.5,
}

