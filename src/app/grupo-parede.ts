import { Parede } from './components/parede/parede.model';

export interface GrupoParede {
  paredeUm: Parede,
  paredeDois: Parede,
  paredeTres: Parede,
  paredeQuatro: Parede,


}
