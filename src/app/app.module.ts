import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ParedeModule } from './components/parede/parede.module';
import { AppService } from './app.service';
import { PainelTintasModule } from './components/painel-tintas/painel-tintas.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ParedeModule,
    FormsModule,
    ReactiveFormsModule,
    PainelTintasModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
