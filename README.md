# CODE CHALLENGE
Projeto para calcular quantidade de tintas a serem utilizadas para pintas uma sala com 4 paredes.

## Tecnologias
- Angular: 11
- TtypeScript: 4.0.2

## Instalação
- Clonar o projeto do repositório `https://github.com/fabioaguir/transportadora-front-end.git` no GitHub,
- Estando com o ambiente configurado para Angular, executar no terminal na pasta do projeto o comando `npm install` para instalar as dependências
- Depois das dependências instaladas, executar no terminal na pasta do projeto o comando `ng server` para executar a aplicação.
- No navegador utilizar o endereço `http://localhost:4200/` para acesso a aplicação.
